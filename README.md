## Avue 打印插件

## Avue官网
[https://avuejs.com](https://avuejs.com)

## 介绍
- [demo](https://avuejs.com/doc/plugins/print-plugins)
- [npm](https://www.npmjs.com/package/avue-plugin-print)
- [git](https://gitee.com/smallweigit/avue-plugin-print)


## 使用
```
1.安装
npm install avue-plugin-print --save

2.导入
import $Print from 'avue-plugin-print'

3.使用

//其中，参数 el 为指定 DOM,'no-print'样式名称可以配置忽略的节点
$Print(el)

```

